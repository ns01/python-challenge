## Python Challenge

```
Without directly modifying the data structures, create a script in either Python or Javascript that
cycles through all the parents and prints to the terminal the proper activities for their child's age
group. When there are no more activities for that parent, print “Curriculum complete!”.

    - Personalize the message output to make it more friendly.
    - Allow users to input new activities & parents before executing the script.
    - Print one activity at a time per parent and continue cycling through until
      all parents have received all their activities.

(Make sure your script accounts for any edge cases in the provided data!)

parents = {
    'Henry': {'childName': 'Calvin', 'age': 1},
    'Ada': {'childName': 'Lily', 'age': 4},
    'Emilia': {'childName': 'Petra', 'age': 2},
    'Biff': {'childName': 'Biff Jr', 'age': 3},
    'Milo': {}
}

activities = [
    {
        'age': 1,
        'activity': [
            'Go outside and feel surfaces.',
            'Try singing a song together.',
            'Point and name objects.'
            ]
    },
    {
        'age': 2,
        'activity': [
            'Draw with crayons.',
            'Play with soundmaking toys or instruments.',
            'Look at family pictures together.'
            ]
    },
    {
        'age': 3,
        'activity': [
            'Build with blocks.',
            'Try a simple puzzle.',
            'Read a story together.'
            ]
    }
]

```


