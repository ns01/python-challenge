#!/usr/bin/python

parents = {
    'Henry': {'childName': 'Calvin', 'age': 1},
    'Ada': {'childName': 'Lily', 'age': 4},
    'Emilia': {'childName': 'Petra', 'age': 2},
    'Biff': {'childName': 'Biff Jr', 'age': 3},
    'Milo': {}
}

activities = [
    {
        'age': 1,
        'activity': [
            'Go outside and feel surfaces.',
            'Try singing a song together.',
            'Point and name objects.'
            ]
    },
    {
        'age': 2,
        'activity': [
            'Draw with crayons.',
            'Play with soundmaking toys or instruments.',
            'Look at family pictures together.'
            ]
    },
    {
        'age': 3,
        'activity': [
            'Build with blocks.',
            'Try a simple puzzle.',
            'Read a story together.'
            ]
    }
]

###

yes = ['Yes', 'yes', 'y', 'Y', 'YES']
no = ['No', 'no', 'n', 'N', 'NO']

def add_more():
    add = input("Add more? [Y/n]: ")
    while add not in yes and add not in no:
        print("Sorry, invalid answer.")
        add = input("Add more? [Y/n]: ")
    else:
        if add in yes:
            new_or_exist(input,parents,activities)
        if add in no:
            return(text_child_activity(input, parents,activities))

def new_or_exist(*args):
    response = input("Hi, are you going to input data for a new parent? [Y/n]: ")
    while response not in yes and response not in no:
        print("Sorry invalid answer")
        response = input("Hi, are you going to input data for a new parent? [Y/n]: ")
    else:
        if response in yes:
            add_new(input, parents, activities)
        elif response in no:
            parents_list = [i.lower() for i in parents]
            l = []
            for k in parents:
                l = k
                parents_list.append(l)
            exist = input("Please enter name of existing parent name: ")
            while exist not in parents_list:
                print("Sorry, we can't find parent name: '{}' in our database. Please try again.".format(exist))
                exist = input("Please enter name of existing parent name: ")
            else:
                exist_parent = parents[exist.title()]
                if len(exist_parent)==0:
                    print("Record found for: '{}'.".format(exist))
                    child_name = input("Enter child name: ")
                    child_age = input("Enter age of child (0-6yrs old): ")
                    age_group = ['0','1','2','3','4','5','6']
                    while child_age not in age_group:
                        print("Sorry, not a valid age.")
                        child_age = (input("Enter age of child (0-6yrs old): "))
                    else:
                        activity_1 = input("Enter first activity: ")
                        activity_2 = input("Enter second activity: ")
                        activity_3 = input("Enter third activity: ")

                        child_age = int(child_age)
                        parents[exist.title()]["childName"] = child_name
                        parents[exist.title()]["age"] = child_age

                        match = next((l for l in activities if l["age"] == child_age), None)
                        if match:
                            new_activities = [activity_1, activity_2, activity_3]
                            for i in new_activities:
                                match["activity"].append(i)
                            add_more()
                        if not match:
                            d = {}
                            d["age"] = child_age
                            d["activity"] = [activity_1, activity_2, activity_3]
                            activities.append(d)
                            add_more()

                elif len(exist_parent)==2:
                    print("Record found for: '{}'.".format(exist))
                    print("\tname of child: {}, age of child: {}".format(exist_parent['childName'],exist_parent['age']))
                    print("Please continue on adding activities.")
                    activity_1 = input("Enter first activity: ")
                    activity_2 = input("Enter second activity: ")
                    activity_3 = input("Enter third activity: ")

                    match = next((l for l in activities if l["age"] == exist_parent['age']), None)
                    if match:
                        new_activities = [activity_1, activity_2, activity_3]
                        for i in new_activities:
                            match["activity"].append(i)
                        add_more()
                    if not match:
                        d = {}
                        d["age"] = exist_parent['age']
                        d["activity"] = [activity_1, activity_2, activity_3]
                        activities.append(d)
                        add_more()

def add_new(*args):
    parents_list = [i.lower() for i in parents]
    parent_name = input("Enter parent name: ")
    while parent_name.lower() in parents_list:
        print("Parent name already taken. Please use a different one.")
        parent_name = input("Enter parent name: ")
    else: 
        parent_name = parent_name.title()
        child_name = input("Enter child name: ")
        child_age = (input("Enter age of child (0-6yrs old): "))
        age_group = ['0','1','2','3','4','5','6']
        while child_age not in age_group:
            print("Sorry, not a valid age.")
            child_age = (input("Enter age of child (0-6yrs old): "))
        else:
            activity_1 = input("Enter first activity: ")
            activity_2 = input("Enter second activity: ")
            activity_3 = input("Enter third activity: ")

        child_age = int(child_age)
        parents[parent_name]={}
        parents[parent_name]["childName"] = child_name
        parents[parent_name]["age"] = child_age

        match = next((l for l in activities if l["age"] == child_age), None)
        if match:
            new_activities = [activity_1, activity_2, activity_3]
            for i in new_activities:
                match["activity"].append(i)
            add_more()
        if not match:
            d = {}
            d["age"] = child_age
            d["activity"] = [activity_1, activity_2, activity_3]
            activities.append(d)
            add_more()

def text_child_activity(*args):
    age_activity_list = [i["age"] for i in activities]
    parent_list = [i for i in parents]
    for k, v in parents.items():
        if len(v)==0:
            v["age"]=None
            v["childName"]=None
        child_age = (v["age"])
        child_name = (v["childName"])
        if child_age not in age_activity_list and child_age is not None:
            print("\nHello " + k + ", there's no activities for " + child_name + " for now. So just sit back and relax. (-:")
            print("\n\n******************************")
        if child_name is None or child_age is None:
            print("\nHello {}, kindly provide more info to give your child the best activities.".format(k))
            print("\n\n******************************")
        if child_age in age_activity_list:
            activity_list = [i['activity'] for i in activities if i['age']==child_age][0]
            index = 0
            print("\nHi " + k + ", It's a great day for " + child_name.title() + " to learn and play!\n\nToday fun activities are:\n")
            while index != len(activity_list):
                print("\t- " + activity_list[index])
                index +=1
            print("\n\t-- Curriculum complete!")
            print("\nGood job {} and {}!\n\n\n******************************".format(k, child_name.title()))

def main():
    new_or_exist(input, parents, activities)

if __name__ == "__main__":
    main()
    
